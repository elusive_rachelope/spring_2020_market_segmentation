filename mylib '\\Client\C$\Users\Rachel\Documents\School\Spring 2020\Practicum I\Exercises\Exercise 4\mylib';
data myfactor;
set mylib.statistical_drivers;
/* do the factor analysis - set eigenval rotate=varimax scree PCA */
proc factor data = myfactor 
maxiter=100
method=principal
mineigen=1 /*when should i poop out a factor?  whenever the eigenvalue is equal to 1 or more. (Kaiser criterion)*/
rotate=varimax /*orthogonal rotation*/
scree
score
print
nfactors=2 /* sets b/c he already knows there's going to be 2 factors, so he forces it to be 2*/
out=myscores; /*poop out into a temporary dataset called myscores*/
var 
try
learn
adventurous
travel
ban
use
pay
packaging;
run;

proc freq data=mycalcs;
tables 
exercise 
suv 
follow 
sponsor 
rei_last_3_mo
age
suv
expedition_unknown;
run;

/* rename the factor variables */
data letscluster;
set myscores;
rename factor1 = adv; /*the two factors should be swapped; this is mislabeled*/
rename factor2 = ecoconscious;
rename my_id = resp_id;
run;
proc fastclus data=letscluster maxclusters=2;
var exercise suv follow sponsor ;
run;
proc fastclus data=letscluster maxclusters=3;
var exercise suv follow sponsor ;
run;
proc fastclus data=letscluster maxclusters=4;
var exercise suv follow sponsor ;
run;
proc fastclus data=letscluster maxclusters=5;
var exercise suv follow sponsor ;
run;
proc fastclus data=letscluster maxclusters=6;
var exercise suv follow sponsor ;
run;
proc fastclus data=letscluster maxclusters=7;
var exercise suv follow sponsor ;
run;
proc fastclus data=letscluster out=myclus maxclusters=3;
var exercise suv follow sponsor ;
run;
proc contents data=myclus;
run;
proc sort data=myclus;
by cluster;
proc means data=myclus;
var rei_last_3_mo
age
suv
expedition_unknown;
by cluster;
run;

