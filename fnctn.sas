filename bigrec 'O:\Student$\MSDA2020\MKT6971_002\Instructor\FA15_Data.txt' lrecl=65378;
/* go get the raw file */
data mydata;
infile bigrec;
input 
my_id 1-7
try_new_things_agree_alot	6657
try_new_things_agree_little	6684
try_new_things_neither	6738
try_new_things_disagree_little	6765
try_new_things_disagree_alot	6792
lrn_abt_thngs_agree_alot	6661
lrn_abt_thngs_agree_little	6688
lrn_abt_thngs_neither	6742
lrn_abt_thngs_disagree_little	6769
lrn_abt_thngs_disagree_alot	6796
adventurous_agree_alot	6667
adventurous_agree_little	6694
adventurous_neither	6748
adventurous_disagree_little	6775
adventurous_disagree_alot	6802
trvl_btn_path_agree_alot	6670
trvl_btn_path_agree_little	6697
trvl_btn_path_neither	6751
trvl_btn_path_disagree_little	6778
trvl_btn_path_disagree_alot	6805
ban_prdcts_agree_alot	7605
ban_prdcts_agree_little	7628
ban_prdcts_neither	7674
ban_prdcts_disagree_little	7697
ban_prdcts_disagree_alot	7720
prods_use_rcycld_agree_alot	7592
prods_use_rcycld_agree_little	7615
prods_use_rcycld_neither	7661
prods_use_rcycld_disagree_little	7684
prods_use_rcycld_disagree_alot	7707
pay_more_agree_alot	7607
pay_more_agree_little	7630
pay_more_neither	7676
pay_more_disagree_little	7699
pay_more_disagree_alot	7722
packaging_agree_alot		4190
packaging_agree_little		4204
packaging_neither			4232
packaging_disagree_little	4246
packaging_disagree_alot		4260
function_agree_alot			3429
function_agree_little		3456
function_neither			3510
function_disagree_little	3537
function_disagree_alot		3564
;	
run;
data mycalcs;
set mydata;
/* use an array to turn missing values to zeros */
array missy(9,5) 
try_new_things_agree_alot
try_new_things_agree_little
try_new_things_neither
try_new_things_disagree_little
try_new_things_disagree_alot
lrn_abt_thngs_agree_alot
lrn_abt_thngs_agree_little
lrn_abt_thngs_neither
lrn_abt_thngs_disagree_little
lrn_abt_thngs_disagree_alot
adventurous_agree_alot
adventurous_agree_little
adventurous_neither
adventurous_disagree_little
adventurous_disagree_alot
trvl_btn_path_agree_alot
trvl_btn_path_agree_little
trvl_btn_path_neither
trvl_btn_path_disagree_little
trvl_btn_path_disagree_alot
ban_prdcts_agree_alot
ban_prdcts_agree_little
ban_prdcts_neither
ban_prdcts_disagree_little
ban_prdcts_disagree_alot
prods_use_rcycld_agree_alot
prods_use_rcycld_agree_little
prods_use_rcycld_neither
prods_use_rcycld_disagree_little
prods_use_rcycld_disagree_alot
pay_more_agree_alot
pay_more_agree_little
pay_more_neither
pay_more_disagree_little
pay_more_disagree_alot
packaging_agree_alot
packaging_agree_little
packaging_neither
packaging_disagree_little
packaging_disagree_alot
function_agree_alot		
function_agree_little	
function_neither		
function_disagree_little
function_disagree_alot;
/* now make missing values zeroes */
do i = 1 to 9;
do j = 1 to 5;
if missy(i,j) = . then missy(i,j) = 0;
end;
end;
/* make array for 8 variable sums */
array mysum(9);	
/* sum up the vars and make no mark or > 1 mark missing*/
/* now make each variable, being sure to ignore zeroes and > 1 */
do k = 1 to 9;
mysum(k) = missy(k,1)+ missy(k,2)+missy(k,3) + missy(k,4) + missy(k,5);
end;
/* now if the variable is not zero or > 1 create var */
array myvar(9);
do m = 1 to 9;
if mysum(m) = 1 then
myvar(m) = (missy(m,1)*5) + (missy(m,2)*4)+ (missy(m,3)*3) + (missy(m,4))*2 + (missy(m,5)*1);
else
myvar(m) = .;
end;
/* now make the var names pretty again */
try = myvar(1);
learn = myvar(2);
adventurous = myvar(3);
travel = myvar(4);
ban = myvar(5);
use = myvar(6);
pay = myvar(7);
packaging = myvar(8);
fnctn = myvar(9);
/* make sum variables available */
mysum1=mysum(1);
mysum2=mysum(2);
mysum3=mysum(3);
mysum4=mysum(4);
mysum5=mysum(5);
mysum6=mysum(6);
mysum7=mysum(7);
mysum8=mysum(8);
mysum9=mysum(9);
run;
/* check incidence of multiple check marks */
proc freq data=mycalcs;
tables 
mysum1
mysum2
mysum3
mysum4
mysum5
mysum6
mysum7
mysum8
mysum9;
run;
PROC EXPORT DATA= WORK.MYCALCS 
            OUTFILE= "\\Client\C$\Users\Rachel\Documents\School\Spring 2
020\Practicum I\Exercises\data\kmeansanalysis_data2.csv" 
            DBMS=CSV REPLACE;
     PUTNAMES=YES;
RUN;
