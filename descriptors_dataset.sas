filename bigrec 'O:\Student$\MSDA2020\MKT6971_002\Instructor\FA15_Data.txt' lrecl=65378;
/* go get the raw file */
data mydata;
infile bigrec;
input 
my_id 1-7
rei_last_3_mo 		43193
white 				2420
female				44200
outdoor_channel_8_9	10863
;	
run;
data mydescriptors;
set mydata;
/* use an array to turn missing values to zeros */
array binary(4) 
rei_last_3_mo
white
female
outdoor_channel_5_7;
/* now make missing values zeroes */
do h = 1 to 4;
if binary(h)=. then binary(h)=0;
end;
/* now make the var names pretty again */
run;
/* check incidence of multiple check marks */
/*PROC EXPORT DATA= WORK.MYCALCS 
            OUTFILE= "\\Client\C$\Users\Rachel\Documents\School\Spring 2
020\Practicum I\Exercises\data\kmeansanalysis_data.csv" 
            DBMS=CSV REPLACE;
     PUTNAMES=YES;
RUN;*/

proc freq data=mycalcs;
tables 
rei_last_3_mo
white
female
outdoor_channel_5_7;
run;

LIBNAME MYLIB '\\Client\C$\Users\Rachel\Documents\School\Spring 2
020\Practicum I\Exercises\data\mylib';

DATA MYLIB.DESCRIPTORS_DATA
;
	SET WORK.MYDESCRIPTORS (keep = rei_last_3_mo white female outdoor_channel_5_7);
RUN;



/*data out.data_save;
    Set data_to_save;
Run;*/
