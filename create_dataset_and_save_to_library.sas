filename bigrec 'O:\Student$\MSDA2020\MKT6971_002\Instructor\FA15_Data.txt' lrecl=65378;
/* go get the raw file */
data mydata;
infile bigrec;
input 
my_id 1-7
try_new_things_agree_alot           6657
try_new_things_agree_little         6684
try_new_things_neither              6738
try_new_things_disagree_little      6765
try_new_things_disagree_alot        6792
lrn_abt_thngs_agree_alot            6661
lrn_abt_thngs_agree_little          6688
lrn_abt_thngs_neither               6742
lrn_abt_thngs_disagree_little       6769
lrn_abt_thngs_disagree_alot         6796
adventurous_agree_alot              6667
adventurous_agree_little            6694
adventurous_neither                 6748
adventurous_disagree_little         6775
adventurous_disagree_alot           6802
trvl_btn_path_agree_alot            6670
trvl_btn_path_agree_little          6697
trvl_btn_path_neither               6751
trvl_btn_path_disagree_little       6778
trvl_btn_path_disagree_alot         6805
ban_prdcts_agree_alot               7605
ban_prdcts_agree_little             7628
ban_prdcts_neither                  7674
ban_prdcts_disagree_little          7697
ban_prdcts_disagree_alot            7720
prods_use_rcycld_agree_alot         7592
prods_use_rcycld_agree_little       7615
prods_use_rcycld_neither            7661
prods_use_rcycld_disagree_little    7684
prods_use_rcycld_disagree_alot      7707
pay_more_agree_alot                 7607
pay_more_agree_little               7630
pay_more_neither                    7676
pay_more_disagree_little            7699
pay_more_disagree_alot              7722
packaging_agree_alot                4190
packaging_agree_little              4204
packaging_neither                   4232
packaging_disagree_little           4246
packaging_disagree_alot             4260
function_agree_alot                 3429
function_agree_little               3456
function_neither                    3510
function_disagree_little            3537
function_disagree_alot              3564
sick_agree_alot                     5152
sick_agree_little                   5201
sick_neither                        5299    
sick_disagree_little                5348
sick_disagree_alot                  5397
exercise_agree_alot                 4624
exericse_agree_little               4701
exercise_neither                    4855
exercise_disagree_litte             4932
exercise_diagree_alot               5009
follow_agree_alot                   6843
follow_agree_little                 6858
follow_neither                      6888
follow_disagree_litte               6903
follow_diagree_alot                 6918
rei_last_3_mo                       43193
white                               2420
female                              44200
outdoor_channel_8_9                 10863
alaska_tv                           12064
expedition_unknown                  12332
doctor_who                          12455
age                                 2764
enjoy_life_agree_alot               4611
enjoy_life_agree_little             4688
enjoy_life_neither                  4842
enjoy_life_disagree_litte           4919
enjoy_life_diagree_alot             4996
tech_agree_alot    					6953
tech_agree_little  					6970
tech_neither       					7004
tech_disagree_litte					7021
tech_diagree_alot  					7038
vacation_agree_alot    				7069
vacation_agree_little  				7076
vacation_neither       				7090
vacation_disagree_litte				7097
vacation_diagree_alot  				7104
unconventional_agree_alot    		6654
unconventional_agree_little  		6681
unconventional_neither       		6735
unconventional_disagree_litte		6762
unconventional_diagree_alot  		6789
suv_agree_alot     					3628
suv_agree_little  					3664
suv_neither       					3736
suv_disagree_litte					3772
suv_diagree_alot  					3808
pay_extra_agree_alot    			4641
pay_extra_agree_little  			4718
pay_extra_neither       			4872
pay_extra_disagree_litte			4949
pay_extra_diagree_alot  			5026
ethically_agree_alot    			4663
ethically_agree_little  			4740
ethically_neither       			4894
ethically_disagree_litte			4971
ethically_diagree_alot				5048
sponsor_agree_alot    				4677
sponsor_agree_little    			4754
sponsor_neither         			4908
sponsor_disagree_litte  			4985
sponsor_diagree_alot				5062
;   

run;
data mycalcs;
set mydata;
/* use an array to turn missing values to zeros */
array missy(20,5) 
try_new_things_agree_alot
try_new_things_agree_little
try_new_things_neither
try_new_things_disagree_little
try_new_things_disagree_alot
lrn_abt_thngs_agree_alot
lrn_abt_thngs_agree_little
lrn_abt_thngs_neither
lrn_abt_thngs_disagree_little
lrn_abt_thngs_disagree_alot
adventurous_agree_alot
adventurous_agree_little
adventurous_neither
adventurous_disagree_little
adventurous_disagree_alot
trvl_btn_path_agree_alot
trvl_btn_path_agree_little
trvl_btn_path_neither
trvl_btn_path_disagree_little
trvl_btn_path_disagree_alot
ban_prdcts_agree_alot
ban_prdcts_agree_little
ban_prdcts_neither
ban_prdcts_disagree_little
ban_prdcts_disagree_alot
prods_use_rcycld_agree_alot
prods_use_rcycld_agree_little
prods_use_rcycld_neither
prods_use_rcycld_disagree_little
prods_use_rcycld_disagree_alot
pay_more_agree_alot
pay_more_agree_little
pay_more_neither
pay_more_disagree_little
pay_more_disagree_alot
packaging_agree_alot
packaging_agree_little
packaging_neither
packaging_disagree_little
packaging_disagree_alot
function_agree_alot     
function_agree_little   
function_neither        
function_disagree_little
function_disagree_alot
sick_agree_alot     
sick_agree_little   
sick_neither        
sick_disagree_little
sick_disagree_alot  
exercise_agree_alot     
exericse_agree_little   
exercise_neither        
exercise_disagree_litte 
exercise_diagree_alot
follow_agree_alot       
follow_agree_little 
follow_neither      
follow_disagree_litte   
follow_diagree_alot
enjoy_life_agree_alot       
enjoy_life_agree_little     
enjoy_life_neither          
enjoy_life_disagree_litte   
enjoy_life_diagree_alot 
tech_agree_alot    				
tech_agree_little  				
tech_neither       				
tech_disagree_litte				
tech_diagree_alot  				
vacation_agree_alot    			
vacation_agree_little  			
vacation_neither       			
vacation_disagree_litte			
vacation_diagree_alot  			
unconventional_agree_alot    	
unconventional_agree_little  	
unconventional_neither       	
unconventional_disagree_litte	
unconventional_diagree_alot  	
suv_agree_alot     				
suv_agree_little  				
suv_neither       				
suv_disagree_litte				
suv_diagree_alot  				
pay_extra_agree_alot    		
pay_extra_agree_little  		
pay_extra_neither       		
pay_extra_disagree_litte		
pay_extra_diagree_alot  		
ethically_agree_alot    		
ethically_agree_little  		
ethically_neither       		
ethically_disagree_litte		
ethically_diagree_alot			
sponsor_agree_alot    			
sponsor_agree_little    		
sponsor_neither         		
sponsor_disagree_litte  		
sponsor_diagree_alot			
 
;
/* now make missing values zeroes */
do i = 1 to 20;
do j = 1 to 5;
if missy(i,j) = . then missy(i,j) = 0;
end;
end;
/* make array for 8 variable sums */
array mysum(20);    
/* sum up the vars and make no mark or > 1 mark missing*/
/* now make each variable, being sure to ignore zeroes and > 1 */
do k = 1 to 20;
mysum(k) = missy(k,1)+ missy(k,2)+missy(k,3) + missy(k,4) + missy(k,5);
end;
/* now if the variable is not zero or > 1 create var */
array myvar(20);
do m = 1 to 20;
if mysum(m) = 1 then
myvar(m) = (missy(m,1)*5) + (missy(m,2)*4)+ (missy(m,3)*3) + (missy(m,4))*2 + (missy(m,5)*1);
else
myvar(m) = .;
end;
/* now make the var names pretty again */
try = myvar(1);
learn = myvar(2);
adventurous = myvar(3);
travel = myvar(4);
ban = myvar(5);
use = myvar(6);
pay = myvar(7);
packaging = myvar(8);
fnctn = myvar(9);
sick = myvar(10);
exercise = myvar(11);
follow = myvar(12);
enjoy_life = myvar(13);
tech = myvar(14);
vacation = myvar(15);
unconventional = myvar(16);
suv = myvar(17);
pay = myvar(18);
ethically = myvar(19);
sponsor = myvar(20);

array binary(8) 
rei_last_3_mo
white
female
outdoor_channel_5_7
alaska_tv
expedition_unknown
doctor_who
age;
/* now make missing values zeroes */
do h = 1 to 8;
if binary(h)^=1 then binary(h)=0;
end;
run;
proc freq data=mycalcs;
tables 
learn
adventurous
travel
ban
use
pay
packaging
fnctn
sick
exercise
follow
enjoy_life
tech
vacation
unconventional
suv
pay
ethically
sponsor
rei_last_3_mo
white
female
outdoor_channel_5_7
alaska_tv
expedition_unknown
doctor_who
age;
run;
/*PROC EXPORT DATA= WORK.MYCALCS 
            OUTFILE= "\\Client\C$\Users\Rachel\Documents\School\Spring 2
020\Practicum I\Exercises\data\kmeansanalysis_data.csv" 
            DBMS=CSV REPLACE;
     PUTNAMES=YES;
RUN;*/

LIBNAME MYLIB '\\Client\C$\Users\Rachel\Documents\School\Spring 2
020\Practicum I\Exercises\Exercise 4\mylib';

DATA MYLIB.STATISTICAL_DRIVERS;
    SET WORK.MYCALCS (keep = try
learn
adventurous
travel
ban
use
pay
packaging
fnctn
sick
exercise
follow
enjoy_life
tech
vacation
unconventional
suv
pay
ethically
sponsor
rei_last_3_mo
white
female
outdoor_channel_5_7
alaska_tv
expedition_unknown
doctor_who
age);
RUN;



/*data out.data_save;
    Set data_to_save;
Run;*/
