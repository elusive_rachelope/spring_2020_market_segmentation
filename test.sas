libname mylib "\\Client\C$\Users\Rachel\Documents\School\Spring 2020\Practicum I\Exercises\data\data.csv";
data myfactor;
set mylib.combo;
/* do the factor analysis - set eigenval rotate=varimax scree PCA */
proc factor data = myfactor 
maxiter=100
method=principal
mineigen=1 /*when should i poop out a factor?  whenever the eigenvalue is equal to 1 or more. (Kaiser criterion)*/
rotate=varimax /*orthogonal rotation*/
scree
score
print
nfactors=2 /* sets b/c he already knows there's going to be 2 factors, so he forces it to be 2*/
out=myscores; /*poop out into a temporary dataset called myscores*/
var try
learn
adventurous
travel
ban
use
pay
packaging;
run;



