filename bigrec 'O:\Student$\MSDA2020\MKT6971_002\Instructor\FA15_Data.txt' lrecl=65378;
/* go get the raw file */
/* WHEN I SHOP, I USUALLY VISIT A VARIETY OF STORES */
data mydata;
infile bigrec;
input 
myid 1-7
amazon_last3months 44789;
run;
/* now put together 5 category variable from single values of the five variables above */
data congeal;
set mydata;
if amazon_last3months = 1 then amazon = 1;
if amazon_last3months = . then amazon = 0;
label amazon = "I have made a purchase at Amazon in the last three months";
run;
proc freq data = congeal;
tables amazon;
run;
